#!/bin/sh

# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")
BASEDIR=$(dirname "$SCRIPTPATH")
cd "$BASEDIR" || exit

java -cp "$BASEDIR/lib/*" -Dlogback.configurationFile="$BASEDIR/etc/logback.xml" -jar "$BASEDIR/lib/${build.finalName}.jar"