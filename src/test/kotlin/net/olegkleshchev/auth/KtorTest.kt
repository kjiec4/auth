package net.olegkleshchev.auth

import io.ktor.client.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.cookies.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.testing.*
import net.olegkleshchev.auth.http.adminEndpointPath
import net.olegkleshchev.auth.http.authentication.authenticationConfigurationModule
import net.olegkleshchev.auth.http.authorization.authorizationConfigurationModule
import net.olegkleshchev.auth.http.content.contentNegotiationModule
import net.olegkleshchev.auth.http.reviewerEndpointPath
import net.olegkleshchev.auth.http.routingModule
import net.olegkleshchev.auth.http.sessions.sessionsConfigurationModule
import net.olegkleshchev.auth.http.userEndpointPath
import net.olegkleshchev.auth.roles.PredefinedRoles
import net.olegkleshchev.auth.roles.service.RoleService
import net.olegkleshchev.auth.users.http.ChangePasswordData
import net.olegkleshchev.auth.users.http.UserCreationData
import net.olegkleshchev.auth.users.service.UserService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.io.TempDir
import org.kodein.di.DirectDI
import org.kodein.di.direct
import org.kodein.di.instance
import java.nio.file.Path
import kotlin.io.path.absolute
import kotlin.io.path.pathString
import kotlin.test.Test
import kotlin.test.assertEquals

class KtorTest {
    @TempDir
    lateinit var dbDir: Path

    private lateinit var userService: UserService
    private lateinit var roleService: RoleService
    private lateinit var directDI: DirectDI

    @BeforeEach
    fun init() {
        val di = DIInitializer.initDI(dbDir.absolute().pathString, 10)
        directDI = di.direct
        initApp(directDI)
        userService = directDI.instance()
        roleService = directDI.instance()
    }

    @Test
    fun `should login with correct credentials`() = testApplication {
        initApplication()
        val client = initClient()

        val login = "normis"
        val password = "pwd"

        client.post("/api/users") {
            contentType(ContentType.Application.Json)
            setBody(UserCreationData(login, password))
        }

        val loginResponse = client.post("/api/login") {
            basicAuth(login, password)
        }
        assertEquals(HttpStatusCode.OK, loginResponse.status, "Unexpected response code for a login request.")
    }

    @Test
    fun `should create user`() = testApplication {
        initApplication()
        val client = initClient()

        val login = "normis"
        val password = "pwd"

        val userCreationResponse = client.post("/api/users") {
            contentType(ContentType.Application.Json)
            setBody(UserCreationData(login, password))
        }
        assertEquals(
            HttpStatusCode.Created,
            userCreationResponse.status,
            "Unexpected response code for a user creation."
        )
    }

    @Test
    fun `should create existing user`() = testApplication {
        initApplication()
        val client = initClient()

        val login = "normis"
        val password = "pwd"

        client.post("/api/users") {
            contentType(ContentType.Application.Json)
            setBody(UserCreationData(login, password))
        }

        val userCreationResponse = client.post("/api/users") {
            contentType(ContentType.Application.Json)
            setBody(UserCreationData(login, password))
        }

        assertEquals(
            HttpStatusCode.BadRequest,
            userCreationResponse.status,
            "Unexpected response code for an existing user creation."
        )

        assertEquals(
            "User already exists.",
            userCreationResponse.bodyAsText(),
            "Unexpected response text for an existing user creation."
        )
    }

    @Test
    fun `should fail to change a role for a missing user`() = testApplication {
        initApplication()
        val client = initClient()

        client.post("/api/login") {
            basicAuth("admin", "admin")
        }

        val changeRoleResponse = client.post("/api/users/normis/role") {
            setBody(PredefinedRoles.allRoles.random().code)
        }

        assertEquals(
            HttpStatusCode.BadRequest,
            changeRoleResponse.status,
            "Unexpected response status for changing a role for a missing user."
        )

        assertEquals(
            "User normis is missing.",
            changeRoleResponse.bodyAsText(),
            "Unexpected response text for changing a role for a missing user."
        )
    }

    @Test
    fun `should change user's role`() = testApplication {
        initApplication()
        val client = initClient()

        val login = "normis"
        val password = "pwd"

        client.post("/api/users") {
            contentType(ContentType.Application.Json)
            setBody(UserCreationData(login, password))
        }

        client.post("/api/login") {
            basicAuth(login, password)
        }

        val noRoleReviewerResponse = client.get(reviewerEndpointPath)
        assertEquals(
            HttpStatusCode.Forbidden,
            noRoleReviewerResponse.status,
            "Unexpected response code for the reviewer endpoint request without appropriate role."
        )

        client.post("/api/login") {
            basicAuth("admin", "admin")
        }

        val changeRoleResponse = client.post("/api/users/normis/role") {
            setBody(PredefinedRoles.reviewer.code)
        }

        assertEquals(
            HttpStatusCode.OK,
            changeRoleResponse.status,
            "Unexpected response code for a change role request."
        )

        client.post("/api/login") {
            basicAuth(login, password)
        }

        val hasRoleReviewerResponse = client.get(reviewerEndpointPath)
        assertEquals(
            HttpStatusCode.OK,
            hasRoleReviewerResponse.status,
            "Unexpected response code for the reviewer endpoint request with appropriate role."
        )
        assertEquals(
            "Reviewer endpoint",
            hasRoleReviewerResponse.bodyAsText(),
            "Unexpected response text for the reviewer endpoint request with appropriate role."
        )
    }

    @Test
    fun `should not login with a wrong password`() = testApplication {
        initApplication()
        val client = initClient()

        val login = "normis"
        val password = "pwd"

        client.post("/api/users") {
            contentType(ContentType.Application.Json)
            setBody(UserCreationData(login, password))
        }

        val loginResponse = client.post("/api/login") {
            basicAuth(login, "pwd2")
        }
        assertEquals(HttpStatusCode.Unauthorized, loginResponse.status, "Unexpected response code for a login request.")
    }

    @Test
    fun `should not login as missing user`() = testApplication {
        initApplication()
        val client = initClient()

        val loginResponse = client.post("/api/login") {
            basicAuth("normis", "pwd")
        }
        assertEquals(HttpStatusCode.Unauthorized, loginResponse.status, "Unexpected response code for a login request.")
    }

    @Test
    fun `should access admin endpoint with suitable role`() = testApplication {
        initApplication()
        val client = initClient()

        client.post("/api/login") {
            basicAuth("admin", "admin")
        }

        val response = client.get(adminEndpointPath)
        assertEquals(HttpStatusCode.OK, response.status, "Unexpected response status code for the admin endpoint.")
        assertEquals("Admin endpoint", response.bodyAsText(), "Unexpected response text for the admin endpoint.")
    }

    @Test
    fun `should access reviewer endpoint with suitable role`() = testApplication {
        initApplication()
        val client = initClient()

        val login = "normis"
        val password = "pwd"

        client.post("/api/users") {
            contentType(ContentType.Application.Json)
            setBody(UserCreationData(login, password))
        }

        client.post("/api/login") {
            basicAuth("admin", "admin")
        }

        client.post("/api/users/normis/role") {
            setBody(PredefinedRoles.reviewer.code)
        }

        client.post("/api/login") {
            basicAuth(login, password)
        }

        val response = client.get(reviewerEndpointPath)
        assertEquals(HttpStatusCode.OK, response.status, "Unexpected response status code for the reviewer endpoint.")
        assertEquals("Reviewer endpoint", response.bodyAsText(), "Unexpected response text for the reviewer endpoint.")
    }

    @Test
    fun `should access user endpoint with suitable role`() = testApplication {
        initApplication()
        val client = initClient()

        val login = "normis"
        val password = "pwd"

        client.post("/api/users") {
            contentType(ContentType.Application.Json)
            setBody(UserCreationData(login, password))
        }

        client.post("/api/login") {
            basicAuth("admin", "admin")
        }

        client.post("/api/users/normis/role") {
            setBody(PredefinedRoles.user.code)
        }

        client.post("/api/login") {
            basicAuth(login, password)
        }

        val response = client.get(userEndpointPath)
        assertEquals(HttpStatusCode.OK, response.status, "Unexpected response status code for the user endpoint.")
        assertEquals("User endpoint", response.bodyAsText(), "Unexpected response text for the user endpoint.")
    }

    @Test
    fun `should fail to change a password if the old one is invalid`() = testApplication {
        initApplication()
        val client = initClient()

        val oldPassword = "admin"
        client.post("/api/login") {
            basicAuth("admin", oldPassword)
        }

        val newPassword = "new password"
        val changePasswordResponse = client.post("/api/account/password") {
            contentType(ContentType.Application.Json)
            setBody(ChangePasswordData("admin123", newPassword))
        }
        assertEquals(
            HttpStatusCode.BadRequest,
            changePasswordResponse.status,
            "Unexpected status for a change password request."
        )
        assertEquals(
            "The old password is invalid.",
            changePasswordResponse.bodyAsText(),
            "Unexpected text for a change password request."
        )
    }

    @Test
    fun `should change a password`() = testApplication {
        initApplication()
        val client = initClient()

        val oldPassword = "admin"
        client.post("/api/login") {
            basicAuth("admin", oldPassword)
        }

        val newPassword = "new password"
        val changePasswordResponse = client.post("/api/account/password") {
            contentType(ContentType.Application.Json)
            setBody(ChangePasswordData(oldPassword, newPassword))
        }
        assertEquals(
            HttpStatusCode.OK,
            changePasswordResponse.status,
            "Unexpected status for a change password request."
        )

        val oldPasswordResponse = client.post("/api/login") {
            basicAuth("admin", oldPassword)
        }

        assertEquals(
            HttpStatusCode.Unauthorized,
            oldPasswordResponse.status,
            "Unexpected response status for logging in with old password."
        )


        val response = client.post("/api/login") {
            basicAuth("admin", newPassword)
        }

        assertEquals(
            HttpStatusCode.OK,
            response.status,
            "Unexpected response status for logging in with changed password."
        )
    }

    @Test
    fun `should logout on logout`() = testApplication {
        initApplication()
        val client = initClient()

        client.post("/api/login") {
            basicAuth("admin", "admin")
        }

        val response = client.get(reviewerEndpointPath)
        assertEquals(HttpStatusCode.OK, response.status, "Unexpected response status code for the reviewer endpoint.")
        assertEquals("Reviewer endpoint", response.bodyAsText(), "Unexpected response text for the reviewer endpoint.")

        val logoutResponse = client.get("/api/logout")
        assertEquals(HttpStatusCode.OK, logoutResponse.status, "Unexpected logout response status code.")

        val noReviewerRoleResponse = client.get(reviewerEndpointPath)
        assertEquals(
            HttpStatusCode.Forbidden,
            noReviewerRoleResponse.status,
            "Unexpected response code for the reviewer endpoint request without appropriate role."
        )
    }

    private fun ApplicationTestBuilder.initClient(): HttpClient {
        val client = createClient {
            install(HttpCookies)
            install(ContentNegotiation) {
                json()
            }
        }
        return client
    }

    private fun ApplicationTestBuilder.initApplication() {
        application {
            sessionsConfigurationModule()
            authenticationConfigurationModule(userService)
            authorizationConfigurationModule(roleService)
            contentNegotiationModule()

            routingModule(userService, roleService)
        }
    }
}