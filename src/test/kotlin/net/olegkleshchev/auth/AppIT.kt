package net.olegkleshchev.auth

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.cookies.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.util.collections.*
import kotlinx.coroutines.*
import net.olegkleshchev.auth.roles.PredefinedRoles
import net.olegkleshchev.auth.users.http.ChangePasswordData
import net.olegkleshchev.auth.users.http.UserCreationData
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import org.junit.platform.commons.logging.LoggerFactory
import java.nio.file.Path
import java.util.*
import java.util.concurrent.Executors
import kotlin.coroutines.CoroutineContext
import kotlin.io.path.absolute
import kotlin.io.path.pathString
import kotlin.time.DurationUnit.SECONDS
import kotlin.time.toDuration

@DisplayName("App")
internal class AppIT {
    private val log = LoggerFactory.getLogger(this.javaClass)

    @TempDir
    lateinit var dbDir: Path

    @Test
    fun `should allow multi-threaded access`() {
        val concurrencyLevel = 10
        val testDuration = 30.toDuration(SECONDS)

        @Suppress("SpellCheckingInspection")
        val defaultPassword = "passw0rd!1"
        val logins = ConcurrentSet<String>()

        val app = App(dbDir.absolute().pathString, concurrencyLevel, 0)
        try {
            app.start(false)
            runBlocking {
                val port = app.getPort()
                log.info { "App's port is $port." }
                log.info { "Starting execution." }

                val requestsDispatcher = Executors.newFixedThreadPool(concurrencyLevel).asCoroutineDispatcher()
                withTimeoutOrNull(testDuration) {
                    withContext(Dispatchers.Default) {
                        launch(CoroutineName("User registration")) {
                            repeat(concurrencyLevel) {
                                registerUser(requestsDispatcher, port, defaultPassword, logins)
                            }
                        }
                        launch(CoroutineName("Endpoint calling")) {
                            repeat(concurrencyLevel) {
                                callRandomEndpointWithRandomUser(logins, port, defaultPassword, requestsDispatcher)
                            }
                        }
                        launch(CoroutineName("Password changing")) {
                            repeat(concurrencyLevel) {
                                changePasswordForRandomUser(logins, port, defaultPassword, requestsDispatcher)
                            }
                        }
                    }
                }
            }
        } finally {
            app.stop()
        }
    }

    private suspend fun changePasswordForRandomUser(
        logins: MutableSet<String>,
        port: Int,
        defaultPassword: String,
        requestsDispatcher: CoroutineContext
    ) {
        withContext(requestsDispatcher) {
            launch {
                while (isActive) {
                    createClient().use { client ->
                        val login = logins.randomOrNull()
                        if (login != null) {
                            val loginResponse = client.post("http://localhost:$port/api/login") {
                                basicAuth(login, defaultPassword)
                            }

                            if (HttpStatusCode.OK == loginResponse.status) {
                                client.post("http://localhost:$port/api/account/password") {
                                    contentType(ContentType.Application.Json)
                                    val newPassword = UUID.randomUUID().toString()
                                    setBody(ChangePasswordData(login, newPassword))
                                    log.info { "Changed a password for $login to $newPassword." }
                                }
                            } else {
                                log.info { "Unsuccessful login by $login." }
                            }
                        }
                    }
                }
            }
        }
    }

    private suspend fun callRandomEndpointWithRandomUser(
        logins: MutableSet<String>,
        port: Int,
        defaultPassword: String,
        requestsDispatcher: CoroutineContext
    ) {
        withContext(requestsDispatcher) {
            launch {
                while (isActive) {
                    createClient().use { client ->

                        val login = logins.randomOrNull()
                        if (login != null) {
                            val loginResponse = client.post("http://localhost:$port/api/login") {
                                basicAuth(login, defaultPassword)
                            }

                            if (HttpStatusCode.OK == loginResponse.status) {
                                val endpoint = PredefinedRoles.endpointsByRoles.values.random()
                                val endpointResponse = client.get("http://localhost:$port$endpoint")
                                if (HttpStatusCode.OK == endpointResponse.status) {
                                    log.info { "Successfully called $endpoint by $login." }
                                } else {
                                    log.info { "Unsuccessfully called $endpoint by $login: ${endpointResponse.status}." }
                                }
                            } else {
                                log.info { "Unsuccessful login by $login." }
                            }
                        }
                    }
                }
            }
        }
    }

    private suspend fun registerUser(
        requestsDispatcher: ExecutorCoroutineDispatcher,
        port: Int,
        defaultPassword: String,
        logins: MutableSet<String>
    ) {
        withContext(requestsDispatcher) {
            launch {
                while (isActive) {
                    val login = UUID.randomUUID().toString()

                    createClient().use { client ->
                        log.info { "Adding a user $login." }
                        val userCreationResponse = client.post("http://localhost:$port/api/users") {
                            contentType(ContentType.Application.Json)
                            setBody(UserCreationData(login, defaultPassword))
                        }

                        client.post("http://localhost:$port/api/login") {
                            basicAuth("admin", "admin")
                        }

                        client.post("http://localhost:$port/api/users/$login/role") {
                            setBody(PredefinedRoles.allRoles.random().code)
                        }

                        if (HttpStatusCode.Created == userCreationResponse.status) {
                            log.info { "Added a user $login." }
                            logins.add(login)
                        } else {
                            log.info { "Unsuccessful user creation: ${userCreationResponse.status}" }
                        }
                    }
                }
            }
        }
    }

    //One needs to create a separate client so not to interfere with cookies from other threads/coroutines.
    private fun createClient(): HttpClient {
        val client = HttpClient(CIO) {

            install(HttpCookies)
            install(ContentNegotiation) {
                json()
            }
        }
        return client
    }
}