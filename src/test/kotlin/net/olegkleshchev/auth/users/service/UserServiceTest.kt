package net.olegkleshchev.auth.users.service

import kotlinx.coroutines.runBlocking
import net.olegkleshchev.auth.DIInitializer
import net.olegkleshchev.auth.database.Migrator
import net.olegkleshchev.auth.roles.PredefinedRoles
import net.olegkleshchev.auth.roles.service.RoleService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.io.TempDir
import org.kodein.di.DI
import org.kodein.di.direct
import org.kodein.di.instance
import java.nio.file.Path
import kotlin.io.path.absolute
import kotlin.io.path.pathString
import kotlin.test.assertFalse
import kotlin.test.assertTrue

@DisplayName("UserService")
internal class UserServiceTest {

    @TempDir
    lateinit var dbDir: Path

    private lateinit var di: DI

    @BeforeEach
    fun init() {
        di = DIInitializer.initDI(dbDir.absolute().pathString, 10)
        di.direct.instance<Migrator>().migrate()
    }

    @Test
    fun `should authenticate created user`() {
        runBlocking {
            val userService = di.direct.instance<UserService>()

            val login = "normis"
            val password = "passwd"
            userService.addUser(login, PredefinedRoles.user.code, password)

            assertTrue { userService.authenticateUser(login, password) }

            val newPassword = "passwd123"
            userService.changeUserPassword(login, password, newPassword)

            assertFalse { userService.authenticateUser(login, password) }
            assertTrue { userService.authenticateUser(login, newPassword) }
        }
    }

    @Test
    fun `should fail to change a password on an invalid old password`() {
        runBlocking {
            val userService = di.direct.instance<UserService>()

            val login = "normis"
            userService.addUser(login, PredefinedRoles.user.code, "passwd")

            assertThrows<InvalidPasswordException> {
                userService.changeUserPassword(login, "password", "passwd123")
            }
        }
    }

    @Test
    fun `should change user's role`() {
        val userService = di.direct.instance<UserService>()
        val roleService = di.direct.instance<RoleService>()

        val login = "normis"
        val password = "passwd"
        runBlocking {
            userService.addUser(login, PredefinedRoles.user.code, password)
            assertFalse(
                roleService.hasRole(login, PredefinedRoles.reviewer.code),
                "Newly created user must not have the reviewer role."
            )
        }

        runBlocking {
            userService.changeUserRole(login, PredefinedRoles.reviewer.code)
            assertTrue(
                roleService.hasRole(login, PredefinedRoles.reviewer.code),
                "User must have the reviewer role after a role change."
            )
        }
    }

    @Test
    fun `should return false on existing user addition`() {
        val userService = di.direct.instance<UserService>()

        val login = "normis"
        val password = "passwd"
        runBlocking {
            assertTrue(
                userService.addUser(login, PredefinedRoles.user.code, password),
                "Unexpected new user addition result."
            )

            assertFalse(
                userService.addUser(login, PredefinedRoles.user.code, password),
                "Unexpected existing user addition result."
            )
        }
    }
}