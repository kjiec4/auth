package net.olegkleshchev.auth.roles.service

import kotlinx.coroutines.runBlocking
import net.olegkleshchev.auth.DIInitializer
import net.olegkleshchev.auth.database.Migrator
import net.olegkleshchev.auth.roles.PredefinedRoles
import net.olegkleshchev.auth.users.service.UserService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import org.kodein.di.DI
import org.kodein.di.direct
import org.kodein.di.instance
import java.nio.file.Path
import kotlin.io.path.absolute
import kotlin.io.path.pathString
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

@DisplayName("RoleService")
internal class RoleServiceTest {

    @TempDir
    lateinit var dbDir: Path

    private lateinit var di: DI

    @BeforeEach
    fun init() {
        di = DIInitializer.initDI(dbDir.absolute().pathString, 10)
        di.direct.instance<Migrator>().migrate()
    }

    @Test
    fun `should find stated role for a user`() {
        val roleService = di.direct.instance<RoleService>()
        assertTrue("\"admin\" must have \"Administrator\" role.") {
            runBlocking {
                roleService.hasRole(
                    "admin",
                    PredefinedRoles.admin.code
                )
            }
        }
    }

    @Test
    fun `shouldn't find role user doesnt have`() {
        val directDI = di.direct
        val roleService = directDI.instance<RoleService>()
        val userService = directDI.instance<UserService>()
        val login = "normis"
        runBlocking {
            userService.addUser(login, PredefinedRoles.user.code, "password")
        }

        assertFalse("Normis shouldn't have \"Administrator\" role.") {
            runBlocking {
                roleService.hasRole(
                    login,
                    PredefinedRoles.reviewer.code
                )
            }
        }
    }

    @Test
    fun `should find chained role for a user`() {
        val roleService = di.direct.instance<RoleService>()
        assertTrue("\"admin\" must have \"User\" role.") {
            runBlocking {
                roleService.hasRole(
                    "admin",
                    PredefinedRoles.user.code
                )
            }
        }
    }

    @Test
    fun `should return role`() {
        val roleService = di.direct.instance<RoleService>()
        runBlocking {
            assertEquals(PredefinedRoles.admin.code, roleService.getRole("admin"), "Unexpected role for the \"admin\'.")
        }
    }
}