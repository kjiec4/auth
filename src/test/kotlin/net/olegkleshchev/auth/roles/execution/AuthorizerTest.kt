package net.olegkleshchev.auth.roles.execution

import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import io.mockk.slot
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.test.assertEquals
import kotlin.test.assertTrue

internal class AuthorizerTest {

    @Test
    fun `should run action when authorized`() {
        val actionMock = mockk<suspend () -> Unit>(relaxed = true)
        val action = Action("user", actionMock)
        runBlocking {
            authorize("normis", action, { _, _ -> true }, {}, {})
        }

        coVerify { actionMock.invoke() }
    }

    @Test
    fun `should not run action when login is null`() {
        val actionMock = mockk<suspend () -> Unit>()
        val authorizationExceptionHandler = mockk<suspend (AuthorizationException) -> Unit>(relaxed = true)
        val authException = slot<AuthorizationException>()
        coEvery {
            authorizationExceptionHandler(capture(authException))
        } answers {}

        val action = Action("user", actionMock)
        runBlocking {
            authorize(null, action, { _, _ -> true }, {}, authorizationExceptionHandler)
        }

        assertEquals("User must be logged in.", authException.captured.message)
        coVerify(exactly = 0) { actionMock.invoke() }
        coVerify { authorizationExceptionHandler.invoke(any()) }
    }

    @Test
    fun `should not run action when unauthorized`() {
        val actionMock = mockk<suspend () -> Unit>()
        val authorizationExceptionHandler = mockk<suspend (AuthorizationException) -> Unit>(relaxed = true)
        val authException = slot<AuthorizationException>()
        coEvery {
            authorizationExceptionHandler(capture(authException))
        } answers {}

        val action = Action("user", actionMock)
        runBlocking {
            authorize("normis", action, { _, _ -> false }, {}, authorizationExceptionHandler)
        }

        assertEquals("Action unauthorized.", authException.captured.message)
        coVerify(exactly = 0) { actionMock.invoke() }
        coVerify { authorizationExceptionHandler.invoke(any()) }
    }

    @Test
    fun `should not run action when authorization function throws na AuthorizationException`() {
        val actionMock = mockk<suspend () -> Unit>()
        val authorizationExceptionHandler = mockk<suspend (AuthorizationException) -> Unit>(relaxed = true)
        val authException = slot<AuthorizationException>()
        coEvery {
            authorizationExceptionHandler(capture(authException))
        } answers {}

        val action = Action("user", actionMock)
        val exceptionMessage = "Everything goes south."
        runBlocking {
            authorize(
                "normis",
                action,
                { _, _ -> throw AuthorizationException(exceptionMessage) },
                {},
                authorizationExceptionHandler
            )
        }

        assertEquals(exceptionMessage, authException.captured.message)
        coVerify(exactly = 0) { actionMock.invoke() }
        coVerify { authorizationExceptionHandler.invoke(any()) }
    }

    @Test
    fun `should run action when action role is null`() {
        val sens = AtomicBoolean(false)

        val action = Action(null) {
            sens.set(true)
        }
        runBlocking {
            authorize("normis", action, { _, _ -> false }, {}, {})
        }

        assertTrue(sens.get())
    }
}