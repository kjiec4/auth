INSERT INTO ROLES (CODE, NAME, CONTAINED_ROLE)
VALUES ('user', 'User', null);

INSERT INTO ROLES (CODE, NAME, CONTAINED_ROLE)
VALUES ('reviewer', 'Reviewer', 'user');

INSERT INTO ROLES (CODE, NAME, CONTAINED_ROLE)
VALUES ('admin', 'Administrator', 'reviewer');
