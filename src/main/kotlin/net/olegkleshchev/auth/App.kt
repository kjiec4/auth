package net.olegkleshchev.auth

import io.ktor.server.engine.*
import io.ktor.server.netty.*
import net.olegkleshchev.auth.database.Migrator
import net.olegkleshchev.auth.http.authentication.authenticationConfigurationModule
import net.olegkleshchev.auth.http.authorization.authorizationConfigurationModule
import net.olegkleshchev.auth.http.content.contentNegotiationModule
import net.olegkleshchev.auth.http.routingModule
import net.olegkleshchev.auth.http.sessions.sessionsConfigurationModule
import net.olegkleshchev.auth.roles.service.RoleService
import net.olegkleshchev.auth.users.service.UserService
import org.kodein.di.DirectDI
import org.kodein.di.direct
import org.kodein.di.instance
import org.slf4j.LoggerFactory

/**
 * System bootstrapper and stopper.
 *
 * @param dbDirectoryPath where to store database files;
 * @param databasePoolSize maximum number of connections in the database pool;
 * @param port http port to listen;
 */
class App(
    private val dbDirectoryPath: String,
    private val databasePoolSize: Int,
    private val port: Int
) {
    companion object {
        @JvmStatic
        val LOG = LoggerFactory.getLogger(App::class.java)!!
    }

    private val server by lazy {
        val kodein = DIInitializer.initDI(dbDirectoryPath, databasePoolSize)

        val directDI = kodein.direct
        initApp(directDI)

        val userService = directDI.instance<UserService>()
        val roleService = directDI.instance<RoleService>()

        embeddedServer(Netty, port) {
            //Configuration
            sessionsConfigurationModule()
            authenticationConfigurationModule(userService)
            authorizationConfigurationModule(roleService)
            contentNegotiationModule()

            //Routing and handling
            routingModule(userService, roleService)
        }
    }

    /**
     * Start the system.
     */
    fun start(wait: Boolean = true) {
        server.start(wait)
        LOG.info("App has been started.")
    }

    /**
     * Stop the system.
     */
    fun stop() {
        server.stop()
        LOG.info("App has been stopped.")
    }

    /**
     * Gets real port is in use.
     *
     * Useful for tests with ephemeral ports.
     */
    suspend fun getPort(): Int {
        return server.resolvedConnectors().first().port
    }
}

fun initApp(directDI: DirectDI) {
    val migrator = directDI.instance<Migrator>()
    migrator.migrate()
}