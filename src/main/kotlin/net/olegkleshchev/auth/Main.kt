package net.olegkleshchev.auth

import com.sksamuel.hoplite.ConfigLoaderBuilder
import com.sksamuel.hoplite.addPathSource
import com.sksamuel.hoplite.addResourceSource
import net.olegkleshchev.auth.conf.Config
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.nio.file.Files
import java.nio.file.Path
import kotlin.io.path.Path
import kotlin.io.path.absolutePathString

private object Main {
    @JvmStatic
    val LOG: Logger = LoggerFactory.getLogger(this.javaClass)
}

fun main() {
    val config = ConfigLoaderBuilder.default()
        .addPathSource(Path("etc/application.toml"), optional = true)
        .addResourceSource("/reference.toml")
        .build()
        .loadConfigOrThrow<Config>()

    App(
        getDatabasePath(config.database.location),
        config.database.poolSize,
        config.httpServer.port
    ).start(true)
}

private fun getDatabasePath(dbPath: Path?): String {
    val dbDirectoryPath = dbPath?.absolutePathString() ?: Files.createTempDirectory("authDB").absolutePathString()

    Main.LOG.info("DB path to use is: {}", dbDirectoryPath)
    return dbDirectoryPath
}
