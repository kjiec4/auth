package net.olegkleshchev.auth.users.http

import kotlinx.html.*
import net.olegkleshchev.auth.roles.PredefinedRoles

fun HTML.generateUserPage(login: String?, role: String?) {
    lang = "en"
    head {
        meta {
            charset = Charsets.UTF_8.name()
        }
        title(content = login ?: "Unknown user")
    }
    body {
        a {
            href = "/logout-front"
            +"Logout"
        }
        br { }
        br { }
        div { +"Endpoints:" }
        div {
            addEndpointLinks(login, role)
        }
    }
}

private fun DIV.addEndpointLinks(
    login: String?,
    role: String?
) {
    if (login != null) {
        var currentRole: String? = role
        var endpoint = if (currentRole != null) PredefinedRoles.endpointsByRoles[currentRole] else null
        while (endpoint != null && currentRole != null) {
            addEndpointLink(endpoint, currentRole)
            val containedRole = PredefinedRoles.rolesByCodes[currentRole]?.containedRole
            currentRole = containedRole?.code
            endpoint = PredefinedRoles.endpointsByRoles[currentRole]
        }
    }
}

private fun DIV.addEndpointLink(endpoint: String?, role: String) {
    if (endpoint != null) {
        div {
            a {
                href = endpoint
                val roleName = PredefinedRoles.rolesByCodes[role]?.name ?: role
                +roleName
            }
        }
    }
}
