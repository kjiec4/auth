package net.olegkleshchev.auth.users.http

import kotlinx.serialization.Serializable

@Serializable
data class ChangePasswordData(val oldPassword: String, val newPassword: String)
