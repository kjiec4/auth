package net.olegkleshchev.auth.users.http

import kotlinx.serialization.Serializable

@Serializable
data class UserCreationData(val login: String, val password: String)
