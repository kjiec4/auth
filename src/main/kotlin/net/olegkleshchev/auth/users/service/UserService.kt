package net.olegkleshchev.auth.users.service

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.core.kotlin.withHandleUnchecked
import org.slf4j.LoggerFactory

/**
 * Means to deal with stored roles in the system.
 */
class UserService(private val db: Jdbi, private val coroutineDispatcher: CoroutineDispatcher) {
    companion object {
        private val LOG = LoggerFactory.getLogger(Companion::class.java)
    }

    /**
     * Add new user to the system.
     *
     * @param role new user's role;
     * @param login new user's login;
     * @param password new user's password;
     */
    suspend fun addUser(login: String, role: String, password: String): Boolean {
        val (hashedPassword, salt) = generatePasswordData(password)
        val added = withContext(coroutineDispatcher) {
            db.withHandleUnchecked { handle ->
                val updated =
                    handle.createUpdate(
                        """
                            MERGE INTO USERS USING (VALUES (:login)) VALS(LOGIN)
                            ON USERS.LOGIN = VALS.LOGIN
                            WHEN NOT MATCHED THEN
                                INSERT (LOGIN, PASSWORD, SALT, ROLE)
                                VALUES (:login, :password, :salt, :role)
                        """.trimIndent()
                    )
                        .bind("login", login)
                        .bind("password", hashedPassword)
                        .bind("salt", salt)
                        .bind("role", role)
                        .execute()

                return@withHandleUnchecked updated > 0
            }
        }
        LOG.debug("User $login has been added.")
        return added
    }

    private fun generatePasswordData(password: String): Pair<ByteArray, ByteArray> {
        val salt = PasswordHasher.generateSalt()
        val hashedPassword = PasswordHasher.hashPassword(password, salt)
        return Pair(hashedPassword, salt)
    }

    /**
     * Check user's credentials.
     *
     * @param login login to check;
     * @param password password to check;
     */
    suspend fun authenticateUser(login: String, password: String): Boolean {
        val passwordAndSalt = withContext(coroutineDispatcher) {
            db.withHandleUnchecked { handle ->
                handle
                    .createQuery("SELECT PASSWORD, SALT FROM USERS WHERE LOGIN = ?")
                    .bind(0, login)
                    .mapToMap()
                    .findOne()
                    .orElse(null)
            }
        }

        if (passwordAndSalt == null) {
            LOG.debug("$login is unauthenticated.")
            return false
        }

        val hashedPassword = PasswordHasher.hashPassword(password, passwordAndSalt["salt"] as ByteArray)
        val contentEquals = hashedPassword.contentEquals(passwordAndSalt["password"] as ByteArray)
        if (LOG.isDebugEnabled) {
            if (contentEquals) {
                LOG.debug("$login is authenticated.")
            } else {
                LOG.debug("$login is unauthenticated.")
            }
        }
        return contentEquals
    }

    /**
     * Check whether user exists.
     *
     * @param login user's login to check;
     */
    fun userExists(login: String): Boolean {
        val exists = db.withHandleUnchecked { handle ->
            handle
                .createQuery("SELECT 1 FROM USERS WHERE LOGIN = ?")
                .bind(0, login)
                .mapTo(Int::class.java)
                .findOne().isPresent
        }
        if (LOG.isDebugEnabled) {
            LOG.debug(
                if (exists) "User $login exists" else "User $login not exists"
            )
        }
        return exists
    }

    /**
     * Assign new password to the user.
     *
     * @param login user to change a password;
     * @param oldPassword old password to verify how is it legit to change a password;
     * @param newPassword password to assign to the user;
     */
    suspend fun changeUserPassword(login: String, oldPassword: String, newPassword: String): Boolean {
        //Don't bother if someone will change password after checking.
        if (authenticateUser(login, oldPassword)) {
            val (hashedPassword, generatedSalt) = generatePasswordData(newPassword)

            val updatesCount =
                withContext(coroutineDispatcher) {
                    db.withHandleUnchecked { handle ->
                        handle
                            .createUpdate("UPDATE USERS SET SALT = ?, PASSWORD = ? WHERE LOGIN = ?")
                            .bind(0, generatedSalt)
                            .bind(1, hashedPassword)
                            .bind(2, login)
                            .execute()
                    }
                }
            LOG.debug("Password update has affected $updatesCount users.")
            return updatesCount > 0
        } else {
            LOG.debug("Old password for the $login is invalid.")
            throw InvalidPasswordException()
        }
    }

    /**
     * Change user's role.
     *
     * @param login user to change a role;
     * @param newRole new role for the user;
     */
    suspend fun changeUserRole(login: String, newRole: String): Boolean {
        return withContext(coroutineDispatcher) {
            val updatesCount = db.withHandleUnchecked { handle ->
                handle
                    .createUpdate("UPDATE USERS SET ROLE = ? WHERE LOGIN = ?")
                    .bind(0, newRole)
                    .bind(1, login)
                    .execute()
            }
            LOG.debug("Role change has affected $updatesCount users.")
            return@withContext updatesCount > 0
        }
    }
}

class InvalidPasswordException : RuntimeException()