package net.olegkleshchev.auth.users.service

import java.security.SecureRandom
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec

private const val SALT_LENGTH = 512
private const val KEY_LENGTH = 512
private const val ITERATIONS_COUNT = 1024
private const val ALGORITHM = "PBKDF2WithHmacSHA512"

object PasswordHasher {
    private val RND = SecureRandom()

    @JvmStatic
    fun generateSalt(): ByteArray {
        //That way base64 string length would be 512 characters
        val salt = ByteArray(SALT_LENGTH)
        RND.nextBytes(salt)

        return salt
    }

    @JvmStatic
    fun hashPassword(password: String, salt: ByteArray): ByteArray {
        val passwordChars = password.toCharArray()

        val spec = PBEKeySpec(passwordChars, salt, ITERATIONS_COUNT, KEY_LENGTH)
        try {
            val secretKeyFactory = SecretKeyFactory.getInstance(ALGORITHM)
            return secretKeyFactory.generateSecret(spec).encoded
        } finally {
            spec.clearPassword()
        }
    }
}