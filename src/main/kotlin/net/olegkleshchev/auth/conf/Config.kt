package net.olegkleshchev.auth.conf

import java.nio.file.Path

/**
 * Top-level application configuration data.
 *
 * @param httpServer - HTTP configuration;
 * @param database = database configuration;
 */
data class Config(val httpServer: HttpServer, val database: Database)

/**
 * HTTP configuration data.
 *
 * @param port - port to listen for HTTP communication.
 */
data class HttpServer(val port: Int)

/**
 * Database configuration data.
 *
 * @param location - database file location;
 * @param poolSize - maximum connection pool size;
 */
data class Database(val location: Path?, val poolSize: Int)