package net.olegkleshchev.auth.http.sessions

import io.ktor.server.auth.*

data class UserSession(val login: String) : Principal
