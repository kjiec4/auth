package net.olegkleshchev.auth.http.sessions

import io.ktor.server.application.*
import io.ktor.server.sessions.*

fun Application.sessionsConfigurationModule() {
    install(Sessions) {
        cookie<UserSession>(
            "user_session",
            SessionStorageMemory() // There must be a better storage for a production-ready code
        ) {
            cookie.path = "/"
            cookie.maxAgeInSeconds = 3600
            //cookie.secure = true //Must be set, but I don't mess with HTTPS ATM.
            cookie.httpOnly = true
            cookie.extensions["Same-Site"] = "Strict"
        }
    }
}