package net.olegkleshchev.auth.http.authentication

import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.sessions.*
import io.ktor.util.pipeline.*
import net.olegkleshchev.auth.http.sessions.UserSession

/**
 * Extract current user login from the PipelineContext.
 */
fun PipelineContext<Unit, ApplicationCall>.getLogin(): String? = this.call.getLogin()

/**
 * Extract current user login from the ApplicationCall.
 */
fun ApplicationCall.getLogin() =
    principal<UserIdPrincipal>()?.name ?: sessions.get(UserSession::class)?.login