package net.olegkleshchev.auth.http.authentication

import kotlinx.html.*

/**
 * Generate login page.
 *
 * @param failed - is last attempt failed;
 */
fun HTML.generateLoginPage(failed: Boolean) {
    lang = "en"
    head {
        meta {
            charset = Charsets.UTF_8.name()
        }
        title {
            +"Login"
        }
    }
    body {
        form {
            action = "/login-front"
            method = FormMethod.post

            label {
                htmlFor = "login"
                +"Login"
            }
            input {
                id = "login"
                name = "login"
                type = InputType.text
            }
            label {
                htmlFor = "password"
                +"Password"
            }
            input {
                id = "password"
                name = "password"
                type = InputType.password
            }
            input {
                type = InputType.submit
                value = "Log In"
            }
            if (failed) {
                div {
                    +"Invalid login or password"
                }
            }
        }
    }
}
