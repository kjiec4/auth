package net.olegkleshchev.auth.http.authentication

import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.sessions.*
import net.olegkleshchev.auth.http.sessions.UserSession
import net.olegkleshchev.auth.users.service.UserService
import org.slf4j.LoggerFactory

const val BASIC_AUTH_PROVIDER_NAME = "auth-basic"
const val SESSION_AUTH_PROVIDER_NAME = "auth-session"
const val FRONT_SESSION_AUTH_PROVIDER_NAME = "auth-session-front"
const val FORM_AUTH_PROVIDER_NAME = "auth-form"

val LOG = LoggerFactory.getLogger("AuthenticationConfigurationModule")!!

/**
 * A Ktor module to configure authentication schemes.
 */
fun Application.authenticationConfigurationModule(userService: UserService) {
    install(Authentication) {
        //Basic auth for the REST
        basic(BASIC_AUTH_PROVIDER_NAME) {
            realm = "Authorization server"
            validate { credentials ->
                validate(userService, credentials.name, credentials.password)
            }
            skipWhen(::checkSkip)
        }
        //Session auth for the frontend interactions
        session<UserSession>(FRONT_SESSION_AUTH_PROVIDER_NAME) {
            validate { session ->
                if (userService.userExists(session.login)) {
                    session
                } else {
                    null
                }
            }
            challenge("/")
            skipWhen(::checkSkip)
        }
        //Session-based authentication for the REST
        session<UserSession>(SESSION_AUTH_PROVIDER_NAME) {
            validate { session ->
                if (userService.userExists(session.login)) {
                    session
                } else {
                    null
                }
            }
            skipWhen(::checkSkip)
        }
        //Form-based authentication for the login page
        form(FORM_AUTH_PROVIDER_NAME) {
            userParamName = "login"
            passwordParamName = "password"
            validate { credentials ->
                validate(userService, credentials.name, credentials.password)
            }
            challenge {
                val login = call.getLogin()
                if (login == null) {
                    if (call.request.path() == "/login-front") {
                        call.respondRedirect("/?failed")
                    } else {
                        call.respondRedirect("/")
                    }
                }
            }
            skipWhen(::checkSkip)
        }
    }
}

/**
 * Check whether auth scheme must be skipped.
 */
private fun checkSkip(call: ApplicationCall): Boolean {
    val userSession = call.sessions.get<UserSession>()
    val skip = (userSession?.login == call.principal<UserIdPrincipal>()?.name
            && (userSession?.login != null || call.principal<UserIdPrincipal>()?.name != null))

    if (LOG.isDebugEnabled) {
        if (skip) {
            LOG.debug("Authentication scheme has been skipped")
        } else {
            LOG.debug("Authentication scheme has not been skipped")
        }
    }
    
    return skip
}

/**
 * Validate authentication request.
 */
private suspend fun ApplicationCall.validate(
    userService: UserService,
    login: String,
    password: String
): UserIdPrincipal? {
    LOG.debug("Authenticating $login")
    return if (userService.authenticateUser(login, password)) {
        sessions.set(UserSession(login))
        val userIdPrincipal = UserIdPrincipal(login)
        LOG.debug("$login has been authenticated")
        userIdPrincipal
    } else {
        sessions.clear<UserSession>()
        LOG.debug("$login has not been authenticated")
        null
    }
}