package net.olegkleshchev.auth.http.content

import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.plugins.contentnegotiation.*

/**
 * A Ktor module to configure content negotiation between a client and a server.
 */
fun Application.contentNegotiationModule() {
    install(ContentNegotiation) {
        json()
    }
}