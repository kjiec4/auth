package net.olegkleshchev.auth.http.authorization

import io.ktor.server.application.*
import net.olegkleshchev.auth.roles.service.RoleService

/**
 * Configure Ktor authorization plugin.
 */
fun Application.authorizationConfigurationModule(roleService: RoleService) {
    install(AuthorizationPlugin) {
        authorizationFunction = roleService::hasRole
    }
}