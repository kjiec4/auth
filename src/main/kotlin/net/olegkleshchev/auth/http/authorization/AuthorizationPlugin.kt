package net.olegkleshchev.auth.http.authorization

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.response.*
import io.ktor.util.*
import io.ktor.util.pipeline.*
import net.olegkleshchev.auth.http.authentication.getLogin
import net.olegkleshchev.auth.roles.execution.Action
import net.olegkleshchev.auth.roles.execution.AuthorizationException
import net.olegkleshchev.auth.roles.execution.authorize

private val authorizationFunctionAttributeKey =
    AttributeKey<suspend (login: String, role: String) -> Boolean>("AuthFunction")

/**
 * A Ktor plugin to simplify authorization code.
 */
val AuthorizationPlugin = createApplicationPlugin(
    name = "AuthorizationPlugin",
    createConfiguration = ::AuthorizationPluginConfiguration
) {
    val authFunction = pluginConfig.authorizationFunction
    pluginConfig.apply {
        on(AuthenticationChecked) { call ->
            call.attributes.put(authorizationFunctionAttributeKey, authFunction)
        }
    }
}

/**
 * The default way to handle authorization failures.
 */
val defaultAuthorizationExceptionHandler: suspend PipelineContext<*, ApplicationCall>.(AuthorizationException) -> Unit =
    { exception ->
        call.respondText(status = HttpStatusCode.Forbidden, text = exception.message ?: "Forbidden")
    }

/**
 * Authorize current user to perform given action.
 *
 * @param action an action to perform;
 * @param authorizationExceptionHandler a way to deal with authorization exceptions;
 */
suspend fun PipelineContext<Unit, ApplicationCall>.authorize(
    action: Action,
    authorizationExceptionHandler: suspend PipelineContext<*, ApplicationCall>.(AuthorizationException) -> Unit = defaultAuthorizationExceptionHandler
) {
    val login = getLogin()
    val authFunction = call.attributes[authorizationFunctionAttributeKey]

    authorize(
        login,
        action,
        authFunction,
        this::proceed
    ) { authorizationException -> this.authorizationExceptionHandler(authorizationException) }
}

class AuthorizationPluginConfiguration {
    var authorizationFunction: suspend (login: String, role: String) -> Boolean = { _: String, _: String -> true }
}