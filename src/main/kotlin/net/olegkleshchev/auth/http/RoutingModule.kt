package net.olegkleshchev.auth.http

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.html.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.sessions.*
import net.olegkleshchev.auth.http.authentication.*
import net.olegkleshchev.auth.http.authorization.authorize
import net.olegkleshchev.auth.http.sessions.UserSession
import net.olegkleshchev.auth.roles.PredefinedRoles
import net.olegkleshchev.auth.roles.execution.Action
import net.olegkleshchev.auth.roles.http.adminAction
import net.olegkleshchev.auth.roles.http.changeRoleAction
import net.olegkleshchev.auth.roles.http.reviewerAction
import net.olegkleshchev.auth.roles.http.userAction
import net.olegkleshchev.auth.roles.service.RoleService
import net.olegkleshchev.auth.users.http.ChangePasswordData
import net.olegkleshchev.auth.users.http.UserCreationData
import net.olegkleshchev.auth.users.http.generateUserPage
import net.olegkleshchev.auth.users.service.InvalidPasswordException
import net.olegkleshchev.auth.users.service.UserService

const val adminEndpointPath = "/api/admin"
const val reviewerEndpointPath = "/api/reviewer"
const val userEndpointPath = "/api/user"

/**
 * A Ktor module to configure request routing.
 */
fun Application.routingModule(userService: UserService, roleService: RoleService) {
    routing {
        authenticate(FORM_AUTH_PROVIDER_NAME) {
            //Handle frontend login
            post("/login-front") {
                val login = getLogin()
                if (login == null) {
                    call.respondRedirect("/?failed")
                } else {
                    call.respondRedirect("/users/$login")
                }
            }
        }
        //User's page
        authenticate(FRONT_SESSION_AUTH_PROVIDER_NAME) {
            get("/users/{login}") {
                val lgn = call.parameters["login"]
                val currentUser = getLogin()
                if (lgn != currentUser) {
                    call.respondText(status = HttpStatusCode.Forbidden) { "Requested page is not for the current user." }
                } else {
                    val role = if (currentUser == null) null else roleService.getRole(currentUser)
                    call.respondHtml(HttpStatusCode.OK) {
                        generateUserPage(currentUser, role)
                    }
                }
            }
        }
        //Handle frontend logout
        get("/logout-front") {
            call.sessions.clear<UserSession>()
            call.respondRedirect("/")
        }

        authenticate(BASIC_AUTH_PROVIDER_NAME) {
            //Handle REST login
            post("/api/login") {
                call.respond(HttpStatusCode.OK)
            }
        }
        authenticate(SESSION_AUTH_PROVIDER_NAME) {
            //Change current user's password
            post("/api/account/password") {
                val (oldPassword, newPassword) = call.receive<ChangePasswordData>()
                val login = getLogin()
                if (login != null) {
                    try {
                        val changed = userService.changeUserPassword(login, oldPassword, newPassword)
                        if (changed) {
                            call.respond(HttpStatusCode.OK)
                        } else {
                            call.respondText(status = HttpStatusCode.BadRequest) { "User $login is missing." }
                        }
                    } catch (e: InvalidPasswordException) {
                        call.respondText(status = HttpStatusCode.BadRequest) { "The old password is invalid." }
                    }
                } else {
                    call.respond(HttpStatusCode.Forbidden)
                }
            }

            get(adminEndpointPath) {
                authorize(adminAction())
            }
            get(reviewerEndpointPath) {
                authorize(reviewerAction())
            }
            get(userEndpointPath) {
                authorize(userAction())
            }

            //Change user's role
            post("/api/users/{login}/role") {
                authorize(changeRoleAction(userService))
            }
        }

        //Handle REST logout
        get("/api/logout") {
            call.sessions.clear<UserSession>()
            call.respond(HttpStatusCode.OK)
        }
        //Add new user
        post("/api/users") {
            //I have left here this action as an example.
            authorize(Action {
                val (login, password) = call.receive<UserCreationData>()
                val added = userService.addUser(login, PredefinedRoles.user.code, password)
                if (added) {
                    call.respond(HttpStatusCode.Created)
                } else {
                    call.respondText(status = HttpStatusCode.BadRequest) { "User already exists." }
                }
            })
        }
        //Login page
        get("/") {
            val failed = call.request.queryParameters["failed"] != null
            call.respondHtml(HttpStatusCode.OK) {
                generateLoginPage(failed)
            }
        }
    }
}
