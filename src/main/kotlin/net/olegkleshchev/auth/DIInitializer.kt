package net.olegkleshchev.auth

import com.zaxxer.hikari.HikariDataSource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.asCoroutineDispatcher
import net.olegkleshchev.auth.database.Migrator
import net.olegkleshchev.auth.roles.service.RoleService
import net.olegkleshchev.auth.users.service.UserService
import org.jdbi.v3.core.Jdbi
import org.kodein.di.DI
import org.kodein.di.bindProvider
import org.kodein.di.bindSingleton
import org.kodein.di.instance
import java.util.concurrent.Executors
import javax.sql.DataSource

/**
 * DI context populator.
 */
object DIInitializer {

    fun initDI(dbDirectoryPath: String, databasePoolSize: Int) = DI {
        bindSingleton {
            val hikariDataSource = HikariDataSource()
            hikariDataSource.jdbcUrl = "jdbc:h2:$dbDirectoryPath/authDB"
            hikariDataSource.maximumPoolSize = databasePoolSize
            return@bindSingleton hikariDataSource
        }

        bindSingleton {
            Jdbi.create(instance<DataSource>()).installPlugins()
        }

        bindProvider { Migrator(instance()) }

        bindSingleton { UserService(instance(), instance()) }
        bindSingleton { RoleService(instance(), instance()) }
        //Dispatcher for accessing the database
        bindSingleton<CoroutineDispatcher> { Executors.newFixedThreadPool(databasePoolSize).asCoroutineDispatcher() }
    }
}