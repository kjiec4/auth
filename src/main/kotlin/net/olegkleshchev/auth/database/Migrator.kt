package net.olegkleshchev.auth.database

import org.flywaydb.core.Flyway
import org.slf4j.LoggerFactory
import javax.sql.DataSource

/**
 * Executor of database migrations.
 *
 * @param dataSource - a data source on which migration must be conducted;
 */
class Migrator(private val dataSource: DataSource) {
    companion object {
        @JvmStatic
        val LOG = LoggerFactory.getLogger(Migrator::class.java)
    }

    /**
     * Run migrations.
     */
    fun migrate() {
        LOG.info("Starting migrations.")
        Flyway
            .configure()
            .dataSource(dataSource)
            .locations("classpath:net/olegkleshchev/auth/database/migrations/")
            .load()
            .migrate()
        LOG.info("Migrations has been completed.")
    }
}