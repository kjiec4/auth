package net.olegkleshchev.auth.database.migrations

import net.olegkleshchev.auth.roles.PredefinedRoles
import net.olegkleshchev.auth.users.service.PasswordHasher
import org.flywaydb.core.api.migration.BaseJavaMigration
import org.flywaydb.core.api.migration.Context

@Suppress("ClassName")//Flyway mandates the naming.
class V3__Create_admin : BaseJavaMigration() {

    override fun migrate(context: Context) {
        val salt = PasswordHasher.generateSalt()
        val hashedPassword = PasswordHasher.hashPassword("admin", salt)

        context
            .connection
            .prepareStatement("INSERT INTO USERS (LOGIN, PASSWORD, \"ROLE\", SALT) VALUES (?,?,?,?)")
            .use {
                it.setString(1, "admin")
                it.setBytes(2, hashedPassword)
                it.setString(3, PredefinedRoles.admin.code)
                it.setBytes(4, salt)
                it.executeUpdate()
            }
    }
}