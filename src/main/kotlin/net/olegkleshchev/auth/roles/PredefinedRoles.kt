package net.olegkleshchev.auth.roles

import net.olegkleshchev.auth.http.adminEndpointPath
import net.olegkleshchev.auth.http.reviewerEndpointPath
import net.olegkleshchev.auth.http.userEndpointPath

object PredefinedRoles {
    //In case of changes, ensure that there are no cycles regarding roles’ containment.
    //One must ensure that roles and corresponding values are initialized in the database (e.g. using migrations).
    val user = Role("user", "User")
    val reviewer = Role("reviewer", "Reviewer", user)
    val admin = Role("admin", "Administrator", reviewer)

    val allRoles = setOf(user, reviewer, admin)

    val rolesByCodes = mapOf(
        user.code to user,
        reviewer.code to reviewer,
        admin.code to admin
    )

    val endpointsByRoles = mapOf(
        admin.code to adminEndpointPath,
        reviewer.code to reviewerEndpointPath,
        user.code to userEndpointPath
    )
}