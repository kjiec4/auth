package net.olegkleshchev.auth.roles.http

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.util.pipeline.*
import net.olegkleshchev.auth.roles.PredefinedRoles
import net.olegkleshchev.auth.roles.execution.Action
import net.olegkleshchev.auth.users.service.UserService

val adminAction: suspend PipelineContext<Unit, ApplicationCall>.() -> Action = {
    Action(PredefinedRoles.admin) {
        call.respondText(status = HttpStatusCode.OK, text = "Admin endpoint")
    }
}

val reviewerAction: suspend PipelineContext<Unit, ApplicationCall>.() -> Action = {
    Action(PredefinedRoles.reviewer) {
        call.respondText(status = HttpStatusCode.OK, text = "Reviewer endpoint")
    }
}

val userAction: suspend PipelineContext<Unit, ApplicationCall>.() -> Action = {
    Action(PredefinedRoles.user) {
        call.respondText(status = HttpStatusCode.OK, text = "User endpoint")
    }
}

val changeRoleAction: suspend PipelineContext<Unit, ApplicationCall>.(userService: UserService) -> Action =
    { userService ->
        Action(PredefinedRoles.admin) {
            val login = call.parameters["login"]
            if (login != null) {
                val changed = userService.changeUserRole(login, call.receiveText())
                if (changed) {
                    call.respond(HttpStatusCode.OK)
                } else {
                    call.respondText(status = HttpStatusCode.BadRequest) { "User $login is missing." }
                }
            } else {
                call.respondText(status = HttpStatusCode.BadRequest) { "Login is missing." }
            }
        }
    }