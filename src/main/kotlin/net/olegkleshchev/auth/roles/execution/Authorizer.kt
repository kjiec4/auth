package net.olegkleshchev.auth.roles.execution

import net.olegkleshchev.auth.roles.Role
import org.slf4j.LoggerFactory

val LOG = LoggerFactory.getLogger("Authorizer")

/**
 * Authorize current user to perform the given action.
 *
 * @param action action to execute;
 * @param authorizationExceptionHandler a way to deal with authorization exceptions;
 * @param login user to authorize;
 * @param authorizationFunction a way to check authorization status;
 * @param proceedFunction a way to normally continue execution if the authorization was successful;
 */
suspend fun authorize(
    login: String?,
    action: Action,
    authorizationFunction: suspend (login: String, role: String) -> Boolean,
    proceedFunction: suspend () -> Unit,
    authorizationExceptionHandler: suspend (AuthorizationException) -> Unit
) {
    val actionRole = action.role

    if (actionRole == null) {
        LOG.debug("No role to check.")
        action.action.invoke()
        return proceedFunction.invoke()
    } else {
        if (login == null) {
            LOG.debug("Missing login.")
            return authorizationExceptionHandler(AuthorizationException("User must be logged in."))
        }

        try {
            if (!authorizationFunction(login, actionRole)) {
                LOG.debug("$login is unauthorized.")
                return authorizationExceptionHandler(AuthorizationException("Action unauthorized."))
            }
            LOG.debug("$login is authorized.")
            action.action.invoke()
            return proceedFunction.invoke()
        } catch (e: AuthorizationException) {
            return authorizationExceptionHandler(e)
        }
    }
}

//Implemented as exception so that one can throw it from the authFunction.
class AuthorizationException(message: String?) : Exception(message)

/**
 * A code to be executed given sufficient authorization role.
 *
 * @param action code block to execute;
 * @param role required role;
 */
class Action(val role: String? = null, val action: suspend () -> Unit) {
    constructor(role: Role, action: suspend () -> Unit) : this(role.code, action)
}