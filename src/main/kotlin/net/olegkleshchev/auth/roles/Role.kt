package net.olegkleshchev.auth.roles

data class Role(val code: String, val name: String, val containedRole: Role? = null)
