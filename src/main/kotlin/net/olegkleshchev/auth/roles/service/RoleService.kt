package net.olegkleshchev.auth.roles.service

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.core.kotlin.withHandleUnchecked
import org.slf4j.LoggerFactory

/**
 * Means to deal with stored roles in the system.
 */
class RoleService(private val db: Jdbi, private val coroutineDispatcher: CoroutineDispatcher) {
    companion object {
        @JvmStatic
        val LOG = LoggerFactory.getLogger(RoleService::class.java)
    }

    private val hasRoleQuery = """
                    WITH RECURSIVE CONTAINED(CODE, CONTAINED_ROLE) AS (SELECT CODE, CONTAINED_ROLE
                                                                       FROM ROLES
                                                                       WHERE CONTAINED_ROLE IS NULL
                                                                       UNION ALL
                                                                       SELECT r.CODE, r.CONTAINED_ROLE
                                                                       FROM ROLES r, USERS u
                                                                                INNER JOIN CONTAINED rc ON r.CONTAINED_ROLE = rc.CODE
                                                                                WHERE u.LOGIN = ?
                                                                                AND r.CONTAINED_ROLE != u.ROLE)
                    SELECT 1 MARK
                    FROM CONTAINED c
                    WHERE c.CODE = ?
                """.trimIndent()

    /**
     * Check whether a user has given role.
     *
     * @param login a user to check;
     * @param roleCode a code of a role to check;
     */
    suspend fun hasRole(login: String, roleCode: String): Boolean {
        return withContext(coroutineDispatcher) {
            val present = db.withHandleUnchecked { handle ->
                handle.createQuery(
                    hasRoleQuery
                )
                    .bind(0, login)
                    .bind(1, roleCode)
                    .mapTo(Int::class.java)
                    .findOne()
            }.isPresent

            if (LOG.isDebugEnabled) {
                if (present) {
                    LOG.debug("$login has role $roleCode")
                } else {
                    LOG.debug("$login don't have role $roleCode")
                }
            }

            return@withContext present
        }
    }

    private val getRoleQuery = """
                    SELECT CODE 
                    FROM ROLES
                        INNER JOIN USERS U on ROLES.CODE = U.ROLE
                    WHERE LOGIN =?
                """.trimIndent()

    /**
     * Get a role given user has.
     *
     * @param login user to get a role;
     */
    suspend fun getRole(login: String): String {
        return withContext(coroutineDispatcher) {
            val role = db.withHandleUnchecked { handle ->
                handle.createQuery(getRoleQuery)
                    .bind(0, login)
                    .mapTo(String::class.java)
                    .one()
            }
            LOG.debug("Role for the $login is $role")
            return@withContext role
        }
    }
}