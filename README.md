# Authorization service

## Building

``mvn -U clean install assembly:single``

Build results will be in the `target/` directory prefixed as `auth-1.0-SNAPSHOT-distr`.

There are 3 distributions:

* Exploded folder;
* ZIP archive;
* TGZ archive;

## Configuring

### Logging

One can configure logging in accordance with logback
documentation using the `etc/logback.xml` file from the distribution.

### Database

Database config is in the `etc/application.toml` under the `database` section.

* `poolSize` - maximum number of database connections in the database connection pool;
* `location` - database file location. If the directory is missing, the application will create it;

### HTTP

HTTP config is in the `etc/application.toml` under the `httpServer` section.

* `port` - an HTTP port the application will listen;

## Running

To run the application, one can execute `bin/start.sh`. Working on the OSes other than Linux is not guaranteed.

## Multithreaded test

It is in the `net.olegkleshchev.auth.AppIT.should allow multi-threaded access`.

One can set concurrency level and test duration at the beginning of the aforementioned test method.

## REST endpoints

### `GET /api/login`

Allows to start the user session and to get a session cookie. One must use obtained session cookie for further requests
to be authenticated and/or authorized.

Authentication credentials must be passed using the HTTP basic auth.

### `POST /api/account/password`

Allows to change the password for the current user.

The old and the new passwords are passed as a JSON:

```json
{
  "oldPassword": "oldePass",
  "newPassword": "newPass"
}
```

### `GET /api/admin`

An endpoint available for users with the Administrator role granted.

### `GET /api/reviewer`

An endpoint available for users with the Reviewer role granted.

### `GET /api/user`

An endpoint available for users with the User role granted.

### `POST /api/users/{login}/role`

Allows to change a role for a user.

A login for the user to change a role is passed as the path parameter `login`. A target role for the user is passed as
a plain text in a request body.

Available only for users with the Administrator role.

### `GET /api/logout`

Allows to end current user session.

### `POST /api/users`

Allows to create a new user.

New user credentials are passed as a JSON:

```json
{
  "login": "newUser",
  "password": "itsPassword"
}
```

### UI

Login page is at the root path `/`. User pages are at `/api/users/{login}`.

### Authorization

Code blocks that should be checked for the authorization have to be wrapped
in `net.olegkleshchev.auth.roles.execution.Action`. Action contains a code block and a code for a role a user must
possess to execute a wrapped code.

There are the `net.olegkleshchev.auth.roles.execution.AuthorizerKt.authorize` function that takes all the parts to check
the authorization and to deal with authorization failures and
the `net.olegkleshchev.auth.http.authorization.AuthorizationPluginKt.authorize` - simplified version to use with Ktor.  
The complicated variant allows to build simplified versions for different authorization contexts.

### Roles

At the moment, there are three predefined roles:

* Administrator;
* Reviewer;
* User;

One role can contain only one other role directly or none. That way there's a chain of
roles `Administrator -> Reviewer -> User`.

### Storage

There's a database for storing users and their roles.

A user database entry stores a login, a hashed password, a hashing salt and a user's role code.

A role database entry stores a role code, a contained role code, and a role name. This table enables the developer to
use non-predefined roles in the future.